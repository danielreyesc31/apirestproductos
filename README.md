﻿## BACK01 - API REST con integración de métodos GET, POST, PUT, DELETE y PATCH y control de respuestas HTTP

El siguiente Codelab integra todas las operaciones HTTP implementadas anteriormente y veremos como manejar la respuesta HTTP de los servicios mediante **ResponseEntity**.

**URL**: localhost:8081/api/TechU/apiRestProductos

Daniel Reyes Castro
